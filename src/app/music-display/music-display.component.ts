import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HobbiesMusicService } from '../hobbies-music/hobbies-music.service';

@Component({
  selector: 'app-music-display',
  templateUrl: './music-display.component.html',
  styleUrls: ['./music-display.component.scss'],
})
export class MusicDisplayComponent implements OnInit, OnDestroy {
  subs = new Subscription(); //this.subs.add()
  albums: any = {};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private albumsService: HobbiesMusicService
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .pipe(
          switchMap((params) => this.albumsService.getOne(params.get('_id')))
        ) //use switchMap to kill other request and take the last request
        .subscribe((album: any) => {
          this.albums = album;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
