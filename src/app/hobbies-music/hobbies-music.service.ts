import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

const apiUrl = (path: string) => `http://localhost:3000/${path}`;

@Injectable({
  providedIn: 'root'
})
export class HobbiesMusicService {

  constructor(private readonly http: HttpClient) {}
  all() {
    console.dir(apiUrl('albums'));
    return this.http.get<any[]>(apiUrl('albums'));
  }

  getOne(id: string) {
    return this.http.get(apiUrl(`albums/${id}`));
  }

  create(album: any) {
    return this.http.post(apiUrl(`albums/`), album);
  }

  update(id: string, album: any) {
    return this.http.patch(apiUrl(`albums/${id}`), album); //we need two params the id and the body
  }

  remove(id: string) {
    return this.http.delete(apiUrl(`albums/${id}`)); //we need two params the id and the body
  }
}
