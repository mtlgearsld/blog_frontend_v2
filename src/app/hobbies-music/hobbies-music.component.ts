import { Component, OnInit } from '@angular/core';
import { HobbiesMusicService } from './hobbies-music.service';

@Component({
  selector: 'app-hobbies-music',
  templateUrl: './hobbies-music.component.html',
  styleUrls: ['./hobbies-music.component.scss'],
})
export class HobbiesMusicComponent implements OnInit {
  albums: any[] = [];
  constructor(private musicService: HobbiesMusicService) {
    this.musicService.all().subscribe((album) => {
      this.albums = album;
    });
  }

  ngOnInit() {}
}
