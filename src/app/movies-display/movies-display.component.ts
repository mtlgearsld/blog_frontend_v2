import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HobbiesMoviesService } from '../hobbies-movies/hobbies-movies.service';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-games',
  templateUrl: './movies-display.component.html',
  styleUrls: ['./movies-display.component.scss'],
})
export class MoviesDisplayComponent implements OnInit, OnDestroy {
  subs = new Subscription();
  movies: any = {};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private moviesService: HobbiesMoviesService
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .pipe(
          switchMap((params) => this.moviesService.getOne(params.get('id')))
        )
        .subscribe((movie: any) => {
          this.movies = movie;
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
