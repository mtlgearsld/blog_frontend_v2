import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// const { apiUrl } = environment; //so we dont repeat typing environment.api.....
const apiUrl = (path: string) => `http://localhost:3000/${path}`;

@Injectable({
  providedIn: 'root'
})
export class MasoudGamesService {
  constructor(private readonly http: HttpClient) {}
  all() {
    console.dir(apiUrl('games'));
    return this.http.get<any[]>(apiUrl('games'));
  }

  getOne(id: string) {
    return this.http.get(apiUrl(`games/${id}`));
  }

  create(game: any) {
    return this.http.post(apiUrl(`games/`), game);
  }

  update(id: string, game: any) {
    return this.http.patch(apiUrl(`games/${id}`), game); //we need two params the id and the body
  }

  remove(id: string) {
    return this.http.delete(apiUrl(`games/${id}`)); //we need two params the id and the body
  }
}
