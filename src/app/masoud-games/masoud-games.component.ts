import { Component, OnInit, Input } from '@angular/core';
import { MasoudGamesService } from './masoud-games.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-games',
  templateUrl: './masoud-games.component.html',
  styleUrls: ['./masoud-games.component.scss'],
})
export class MasoudGamesComponent implements OnInit {
  games: any[] = [];
  constructor(private gameService: MasoudGamesService) {
    this.gameService.all().subscribe((games) => {
      this.games = games;
      // console.log(sortGames);
    });
  } //dependance injection

  ngOnInit() {}
}
