import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MasoudGamesService } from '../masoud-games/masoud-games.service';

@Component({
  selector: 'app-game-display',
  templateUrl: './game-display.component.html',
  styleUrls: ['./game-display.component.scss'],
})
export class GameDisplayComponent implements OnInit, OnDestroy {
  subs = new Subscription(); //this.subs.add()
  games: any = {};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private gamesService: MasoudGamesService
  ) {}

  ngOnInit() {
    this.subs.add(
      this.route.paramMap
        .pipe(switchMap((params) => this.gamesService.getOne(params.get('id')))) //use switchMap to kill other request and take the last request
        .subscribe((game: any) => {
          this.games = game;
          console.log(game);
        })
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
