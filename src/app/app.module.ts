import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AboutPageComponent } from './about-page/about-page.component';
import { EducationPageComponent } from './education-page/education-page.component';
import { EntertainmentPageComponent } from './entertainment-page/entertainment-page.component';
import { EntertainmentSelectionComponent } from './entertainment-selection/entertainment-selection.component';
import { GameDisplayComponent } from './game-display/game-display.component';
import { HobbiesMoviesComponent } from './hobbies-movies/hobbies-movies.component';
import { HobbiesMusicComponent } from './hobbies-music/hobbies-music.component';
import { MasoudGamesComponent } from './masoud-games/masoud-games.component';
import { MoviesDisplayComponent } from './movies-display/movies-display.component';
import { MusicDisplayComponent } from './music-display/music-display.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    AboutPageComponent,
    EducationPageComponent,
    EntertainmentPageComponent,
    EntertainmentSelectionComponent,
    GameDisplayComponent,
    HobbiesMoviesComponent,
    HobbiesMusicComponent,
    MasoudGamesComponent,
    MoviesDisplayComponent,
    MusicDisplayComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [LandingPageComponent], //FIXME:
  bootstrap: [AppComponent],
})
export class AppModule {}
