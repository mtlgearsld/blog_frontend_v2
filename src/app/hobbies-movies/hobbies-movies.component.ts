import { Component, OnInit, Input } from '@angular/core';
import { HobbiesMoviesService } from './hobbies-movies.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-hobbies-movies',
  templateUrl: './hobbies-movies.component.html',
  styleUrls: ['./hobbies-movies.component.scss'],
})
export class HobbiesMoviesComponent implements OnInit {
  movies: any[] = [];
  constructor(private movieService: HobbiesMoviesService) {
    this.movieService.all().subscribe((movies) => {
      this.movies = movies;
      // console.log(sortGames);
    });
  } //dependance injection

  ngOnInit() {}
}
