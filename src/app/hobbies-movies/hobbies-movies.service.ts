import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// const { apiUrl } = environment; //so we dont repeat typing environment.api.....
const apiUrl = (path: string) => `http://localhost:3000/${path}`;

@Injectable({
  providedIn: 'root'
})
export class HobbiesMoviesService {
  constructor(private readonly http: HttpClient) {}
  all() {
    console.dir(apiUrl('movies'));
    return this.http.get<any[]>(apiUrl('movies'));
  }

  getOne(id: string) {
    return this.http.get(apiUrl(`movies/${id}`));
  }

  create(movie: any) {
    return this.http.post(apiUrl(`movies/`), movie);
  }

  update(id: string, movie: any) {
    return this.http.patch(apiUrl(`movies/${id}`), movie); //we need two params the id and the body
  }

  remove(id: string) {
    return this.http.delete(apiUrl(`movies/${id}`)); //we need two params the id and the body
  }
}