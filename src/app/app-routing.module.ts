import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutPageComponent } from './about-page/about-page.component';
import { EducationPageComponent } from './education-page/education-page.component';
import { EntertainmentSelectionComponent } from './entertainment-selection/entertainment-selection.component';
import { GameDisplayComponent } from './game-display/game-display.component';
import { HobbiesMoviesComponent } from './hobbies-movies/hobbies-movies.component';
import { HobbiesMusicComponent } from './hobbies-music/hobbies-music.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { MasoudGamesComponent } from './masoud-games/masoud-games.component';
import { MoviesDisplayComponent } from './movies-display/movies-display.component';
import { MusicDisplayComponent } from './music-display/music-display.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'aboutPage', component: AboutPageComponent },
  { path: 'education', component: EducationPageComponent },
  {
    path: 'entertainment-selection',
    component: EntertainmentSelectionComponent,
  },
  { path: 'masoud-games', component: MasoudGamesComponent },
  { path: 'masoud-games/game-display/:id', component: GameDisplayComponent },
  { path: 'hobbies-movies', component: HobbiesMoviesComponent },
  {
    path: 'hobbies-movies/movie-display/:id',
    component: MoviesDisplayComponent,
  },
  { path: 'hobbies-music', component: HobbiesMusicComponent },
  {
    path: 'hobbies-music/music-display/:_id',
    component: MusicDisplayComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
